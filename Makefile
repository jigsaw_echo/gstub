IDIR =.
CC=gcc
LD=gcc

CFLAGS=-I$(IDIR)
CFLAGS += -ggdb -O0 -Wall -Werror -Wno-unused -DDEBUG
LDFLAGS=

ODIR=obj
LIBS=


_DEPS =

DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

SRC = serv.c stub.c

_OBJ = $(SRC:.c=.o)
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

$(ODIR)/%.o: %.c $(DEPS)
		$(CC) -c -o $@ $< $(CFLAGS)

gstub: $(OBJ)
		$(LD) -o $@ $^ $(LDFLAGS) $(LIBS)

.PHONY: clean

clean:
		rm -f $(ODIR)/*.o *~ core core.* $(IDIR)/*~
