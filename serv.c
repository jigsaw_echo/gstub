#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/wait.h>

#include "stub.h"

#define BUF_SIZE 128

int main(int argc, char **argv)
{
    int listenfd, connfd, n, st;
    struct sockaddr_in servaddr, cliaddr;
    socklen_t clilen;
    pid_t child;
    static char msg[BUF_SIZE];

    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(23456);
    bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr));

    printf("listening on port 23456\n");
    listen(listenfd, 5);

    clilen = sizeof(cliaddr);
    connfd = accept(listenfd, (struct sockaddr *) &cliaddr, &clilen);

    if ((child = fork()) == 0) {
        close(listenfd);

        while (1) {
            char *resp;
            int i;

            n = recvfrom(connfd,
                         msg,
                         sizeof(msg),
                         0,
                         (struct sockaddr *) &cliaddr,
                         &clilen);

            if (n <= 0) {
                printf("remote disconnected\n");
                return 0;
            }

            for (i = 0; i < n; i++) {
                int len = 0;

                resp = ugetc(msg[i], &len);
                if (resp)
                    sendto(connfd,
                           resp,
                           len,
                           0,
                           (struct sockaddr *) &cliaddr,
                           sizeof(cliaddr));
            }
        }
    }

    waitpid(child, &st, WUNTRACED);

    return 0;
}
