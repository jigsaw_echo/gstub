#ifdef DEBUG
#include <stdio.h>
#endif

#ifndef NULL
#define NULL    ((void *) 0)
#endif

#define BUF_SIZE 128


static const char hexdigits[] = "0123456789abcdef";

static char *
reply(char *msg, int *rlen, int no_ack)
{
    static char rbuf[BUF_SIZE];
    unsigned char cksum = 0;
    int i;
    char *p = rbuf;

    if (!no_ack)
        *p++ = '+';

    *p++ = '$';
    for (i = 0; msg[i]; i++) {
        *p++ = msg[i];
        cksum += msg[i] % 256;
    }
    *p++ = '#';
    *p++ = hexdigits[(cksum & 0xf0) >> 4];
    *p++ = hexdigits[cksum & 0xf];
    *p = 0;

    *rlen = no_ack ? i + 4 : i + 5;

#ifdef DEBUG
    printf("answering: %s\n", rbuf);
#endif

    return rbuf;
}

/* wrapper for b2h */
#define _b2h(b, l) b2h((unsigned char *)(b), (l))

/* bytes to hex */
static char *
b2h(unsigned char *bytes, int len)
{
    static char bbuf[BUF_SIZE];
    int i, j;
    unsigned char b;

    for (i = 0, j = 0; i < len; i++, j += 2) {
        b = bytes[i];
        bbuf[j] = hexdigits[(b & 0xf0) >> 4];
        bbuf[j + 1] = hexdigits[b & 0xf];
    }
    bbuf[j] = 0;

    return bbuf;
}

/* read int */
static unsigned int
h2i(char *hex, int len, char **pnext)
{
    unsigned int r;
    int i, s;
    unsigned char ch;

#define _ishex(c) (((c) >= '0' && (c) <= '9') || ((c) >= 'a' && (c) <= 'f'))
#define _c2b(c) (unsigned char)((c) <= '9' ? (c) - '0' : 10 + (c) - 'a')

    i = 0;
    if (len > 8)
        len = 8;

    while (i < len) {
        if (!_ishex(hex[i]))
            break;
        ++i;
    }

    if (pnext)
        *pnext = hex + i;

    r = 0;
    s = 0;
    --i;

    while (i >= 0) {
        ch = hex[i--];
        r |= _c2b(ch) << s;
        s += 4;
    }

    return r;
}

static int register_bank[] = {
    0xdeadbeef, 0x00000001, 0x00000002, 0x00000003,
    0x00000004, 0x00000005, 0x00000006, 0x00000007,
    0x00000008, 0x00000009, 0x0000000a, 0x0000000b,
    0x0000000c, /* up to here are GPRs */
    0x11223300, /* R13 SP */
    0x22334400, /* R14 LR */
    0x33445500, /* R15 PC */
};

static int xpsr = 0x00000000;

static int sram[] = { 0x00000001 };

static char *
parse_pkt(char *msg, int len, int *rlen)
{
    static char set_no_ack = 0;
    int no_ack;

    no_ack = set_no_ack;

    /* Supported pkts are:
     * ?
     * D
     * g
     * k
     * m addr,length
     * p n
     *
     * and General Query Packets:
     * qSupported
     * QStartNoAckMode
     *
     */

#ifdef DEBUG
    msg[len] = 0;
    printf("parsing pkt %s\n", msg);
#endif

#define _REPLY(s) reply((s), rlen, no_ack)

    switch (msg[0]) {
    case '?':
        return _REPLY("T06");

    case 'D':
        return _REPLY("OK");

    case 'g':
        return _REPLY(_b2h(register_bank, 13 * sizeof(int)));

    /* Kill needs no request */
    case 'k':
        return NULL;

    case 'm':
    {
        int maddr, mlen;
        char *p;

        maddr = h2i(msg + 1, len - 1, &p);
        len -= (p - msg);
        mlen = h2i(p + 1, len - 1, NULL);
#ifdef DEBUG
        printf("reading memory 0x%x, %d\n", maddr, mlen);
#endif

        return _REPLY(_b2h(sram, mlen));
    }

    case 'p':
    {
        int regnum = h2i(msg + 1, len - 1, NULL);

#ifdef DEBUG
        printf("reading register %d\n", regnum);
#endif
        if (regnum == 25)
            return _REPLY(_b2h(&xpsr, sizeof(int)));
        return _REPLY(_b2h(&register_bank[regnum], sizeof(int)));
    }

    case 'q':
        /* qSupported */
        if (len > 10 && msg[1] == 'S' && msg[2] == 'u' && msg[3] == 'p')
            return _REPLY("QStartNoAckMode+");

        break;

    case 'Q':
        /* QStartNoAckMode */
        if (len == 15 && msg[1] == 'S' && msg[2] == 't' && msg[3] == 'a' &&
            msg[4] == 'r' && msg[5] == 't') {
            set_no_ack = 1;
            return _REPLY("OK");
        }
        break;

    default:
        break;
    }

    /* empty pkt being innocent */
    return _REPLY("");
#undef _REPLY
}

char *
ugetc(char c, int *rlen)
{
    static char ubuf[BUF_SIZE];
    static char pkt_stat = 0; /* 0 - init, 1 - started, 2 - ended */
    static char cksum_cnt = 0;
    static short offset = 0;

    char *result = NULL;

    switch (c) {
    case '$': pkt_stat = 1;
    case '+':   /* ignore Ack */
    case '-':   /* we have nothing to retransmit */
        return NULL;

    case '#':   /* need to wait for 2 more digits the checksum */
        pkt_stat = 2;
        return NULL;

    default:
        break;
    }

    if (pkt_stat == 0)
        return NULL;

    if (pkt_stat == 1) {
        ubuf[offset++] = c;
        return NULL;
    }

    cksum_cnt++;
    if (cksum_cnt == 2) {
        result = parse_pkt(ubuf, offset, rlen);
        offset = 0;
        pkt_stat = 0;
        cksum_cnt = 0;

        return result;
    }

    return NULL;
}

